# quelques infos sur les tests robot effectués

## on teste le produit suivant :
* on teste le **Harbor** suivant : https://demo.goharbor.io
* c'est un serveur de demo que l'on trouve sur internet et qui est réinitialisé tous les 2 jours /!\
* il faut se créer un compte personnel
* ce compte est créé par le biais d'un test robot de **setup** d'environnement, lancé à partir du répertoire zog :
    * en mode GUI : robot --variablefile tests/harbor\_parameters.yaml --outputdir ../test\_results/ tests/suites/harbor-gui/harbor-account-creation.robot
    * en mode non-GUI : xvfb-run robot --variablefile tests/harbor\_parameters.yaml --outputdir ../test\_results/ tests/suites/harbor-gui/harbor-account-creation.robot
* les tests robot sont stockés sous ce gitlab
* pour tester le GUI de **harbor** à partir du répertoire zog :
    * pour le **setup** : robot --variablefile tests/harbor\_parameters.yaml --outputdir ../test\_results/ --include setup tests/suites/harbor-cli/harbor-cli.robot
    * pour les **tests** : robot --variablefile tests/harbor\_parameters.yaml --outputdir ../test\_results/ tests/suites/harbor-gui/harbor-gui.robot
    * pour le **teardown** : robot --variablefile tests/harbor\_parameters.yaml --outputdir ../test\_results/ --include teardown tests/suites/harbor-cli/harbor-cli.robot
* pour tester le CLI de **harbor** à partir du répertoire zog :
    * robot --variablefile tests/harbor\_parameters.yaml --outputdir ../test\_results/ tests/suites/harbor-cli/harbor-cli.robot
* pour tester le CLI de **portainer** à partir du répertoire zog :
    * robot --variablefile tests/portainer\_parameters.yaml --outputdir ../test\_results/ tests/suites/portainer-cli/portainer-cli.robot

