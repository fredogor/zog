*** Settings ***
Library    OperatingSystem
Library    String

*** Keywords ***
Create Initial Admin Account
    [Arguments]    ${portainer_url}    ${login}    ${passwd}
    [Documentation]    to create initial account
    ${output}=    Run    http --ignore-stdin POST ${portainer_url}/api/users/admin/init Username=${login} Password=${passwd} | jq
    Log    ${output}
    [return]    ${output}

Log In Portainer
    [Arguments]    ${portainer_url}    ${login}    ${passwd}
    [Documentation]    to create initial account
    ${output}=    Run    http --ignore-stdin POST ${portainer_url}/api/auth Username=${login} Password=${passwd} | jq --raw-output .[]
    Log    ${output}
    [return]    ${output}

Add An Endpoint In Portainer
    [Arguments]    ${portainer_url}    ${token}    ${name}    ${url}
    [Documentation]    to create initial account
    ${output}=    Run    http --ignore-stdin --form POST ${portainer_url}/api/endpoints "Authorization: Bearer ${token}" Name=${name} URL=${url} EndpointCreationType=1 | jq
    Log    ${output}
    [return]    ${output}

List All The Endpoints In Portainer
    [Arguments]    ${portainer_url}    ${token}
    [Documentation]    to list endpoints
    ${output}=    Run    http --ignore-stdin --form GET ${portainer_url}/api/endpoints "Authorization: Bearer ${token}" | jq
    Log    ${output}
    [return]    ${output}

List All The Names Of The Endpoints In Portainer
    [Arguments]    ${portainer_url}    ${token}
    [Documentation]    to list endpoints
    ${output}=    Run    http --ignore-stdin --form GET ${portainer_url}/api/endpoints "Authorization: Bearer ${token}" | jq .[].Name
    Log    ${output}
    [return]    ${output}

List All The Ids Of The Endpoints In Portainer
    [Arguments]    ${portainer_url}    ${token}
    [Documentation]    to list endpoints
    ${output}=    Run    http --ignore-stdin --form GET ${portainer_url}/api/endpoints "Authorization: Bearer ${token}" | jq .[].Id
    Log    ${output}
    [return]    ${output}

List An Endpoint In Portainer
    [Arguments]    ${portainer_url}    ${token}    ${id}
    [Documentation]    to list endpoints
    ${output}=    Run    http --ignore-stdin --form GET ${portainer_url}/api/endpoints/${id} "Authorization: Bearer ${token}" | jq
    Log    ${output}
    [return]    ${output}
    
Get Endpoint Id From Endpoint Name In Portainer
    [Arguments]    ${portainer_url}    ${token}    ${endpoint_name}
    [Documentation]    to get id of an endpoint name
    ${ids}=    Run    http --ignore-stdin --form GET ${portainer_url}/api/endpoints "Authorization: Bearer ${token}" | jq .[].Id
    Log    ${ids}
    ${names}=    Run    http --ignore-stdin --form GET ${portainer_url}/api/endpoints "Authorization: Bearer ${token}" | jq .[].Name
    Log    ${names}
    
    @{IDS}=    Split String    ${ids}
    @{NAMES}=    Split String    ${names}
    
    ${i}    Set variable    ${0}
    FOR    ${name}    IN    @{NAMES}
        IF    ${name} == '${endpoint_name}'
        Run keyword    Log    ${name}
        Run keyword    Log    ${i}
        ${id}    Set variable    ${i}
        END
    ${i}    Set variable    ${i + 1}
    END
    Log    ${id}
    Log    ${ids}[${id}]
    [return]    ${ids}[${id}]

Get Id Of An Endpoint In Portainer
    [Arguments]    ${portainer_url}    ${token}    ${i}
    [Documentation]    to get id of an endpoint
    ${id}=    Run    http --ignore-stdin --form GET ${portainer_url}/api/endpoints "Authorization: Bearer ${token}" | jq .[${i}].Id
    Log    ${id}
    [return]    ${id}

Remove An Endpoint In Portainer
    [Arguments]    ${portainer_url}    ${token}    ${id}
    [Documentation]    to remove an endpoint
    ${output}=    Run    http --ignore-stdin --form DELETE ${portainer_url}/api/endpoints/${id} "Authorization: Bearer ${token}" | jq
    Log    ${output}
    [return]    ${output}

List All The Containers Of An Endpoint In Portainer
    [Arguments]    ${portainer_url}    ${token}    ${id}
    [Documentation]    to list endpoints
    ${output}=    Run    http --ignore-stdin GET ${portainer_url}/api/endpoints/${id}/docker/containers/json "Authorization: Bearer ${token}" all==true | jq
    Log    ${output}
    [return]    ${output}

