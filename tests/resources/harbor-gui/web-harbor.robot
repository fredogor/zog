*** Settings ***
Documentation    A resource file with Web page related keywords, dedicated to Harbor GUI.
Resource         web.robot

*** Variables ***
${tempo}    3sec

*** Keywords ***
Input Username
    [Arguments]    ${username}
    Wait For Locator And Input Text    xpath://*[@id="login_username"]    ${username}

Input Password
    [Arguments]    ${password}
    Wait For Locator And Input Text    xpath://*[@id="login_password"]    ${password}

Input Project Name
    [Arguments]    ${projectname}
    Wait For Locator And Input Text    xpath://*[@id="create_project_name"]    ${projectname}

Input Selected Project Name
    [Arguments]    ${selectedprojectname}
    Wait For Locator And Input Text    xpath:/html/body/harbor-app/harbor-shell/clr-main-container/div/div/projects/div/div/div[2]/div/hbr-filter/span/input    ${selectedprojectname}

Input Selected Repository Name
    [Arguments]    ${selectedrepositoryname}
    Wait For Locator And Input Text    xpath:/html/body/harbor-app/harbor-shell/clr-main-container/div/div/project-detail/hbr-repository-gridview/div/div[1]/div/div/div/hbr-filter/span/input    ${selectedrepositoryname}

Login to GUI Harbor
    [Arguments]    ${user}    ${password}
    Wait For Locator And Input Text    xpath://*[@id="login_username"]    ${user}    1m
    Wait For Locator And Input Text    xpath://*[@id="login_password"]    ${password}
    Sleep    ${tempo}
    Wait For Locator And Click On Button    xpath://button[contains(.,'LOG IN')]

Perform Admin Operations Of Harbor
    [Arguments]    ${text}
    Click Element    xpath:/html/body/harbor-app/harbor-shell/clr-main-container/navigator/clr-header/div[3]/clr-dropdown[2]
    Sleep    ${tempo}
    Expect Popup    ${text}

Logout GUI Harbor
    Perform Admin Operations Of Harbor    Log Out

Get Harbor Version
    Perform Admin Operations Of Harbor    About
    Sleep    ${tempo}
    Click Element    xpath:/html/body/harbor-app/harbor-shell/about-dialog/clr-modal/div/div[1]/div[2]/div/div[3]/button
    Sleep    ${tempo}

Get Harbor User Profile
    Perform Admin Operations Of Harbor    User Profile
    Sleep    ${tempo}
    Click Element    xpath://*[@id="cancel-btn"]
    Sleep    ${tempo}

Change Harbor Admin Password
    Perform Admin Operations Of Harbor    Change Password
    Sleep    ${tempo}
    Click Element    xpath://*[@id="cancel-btn"]
    Sleep    ${tempo}

Create A Harbor Project
    [Arguments]    ${name}    ${level}=public
    Click Element    xpath:/html/body/harbor-app/harbor-shell/clr-main-container/div/div/projects/div/div/list-project/clr-datagrid/clr-dg-action-bar/button[1]
    Sleep    ${tempo}
    Expect Popup    New Project
    Input Project Name    ${name}
    Sleep    ${tempo}
    Run Keyword If    "${level}" == "public"    Click Element    xpath://*[@id="create_project_public"]
    Sleep    ${tempo}
    Click Element    xpath://*[@id="new-project-ok"]
    Wait Until Element Contains    xpath:/html/body/harbor-app/harbor-shell/clr-main-container/div/div/global-message/div/clr-alert    Created project successfully.    3sec
    Sleep    ${tempo}

Remove A Harbor Project
    [Arguments]    ${name}
    Click Element    xpath://*[@id="delete-project"]
    Sleep    ${tempo}
    Expect Popup    Confirm remove project
    Sleep    ${tempo}
    Click Element    xpath:/html/body/harbor-app/harbor-shell/global-confirmation-dialog/clr-modal/div/div[1]/div[2]/div/div[3]/button[2]
    Wait Until Element Contains    xpath:/html/body/harbor-app/harbor-shell/clr-main-container/div/div/global-message/div/clr-alert    Deleted successfully    3sec
    Sleep    ${tempo}

Select A Harbor Project
    [Arguments]    ${name}
    Click Element    xpath:/html/body/harbor-app/harbor-shell/clr-main-container/div/div/projects/div/div/div[2]/div/hbr-filter/span/clr-icon    
    Sleep    ${tempo}
    Input Selected Project Name    ${name}
    Sleep    ${tempo}
    
    # pour la version v.2.3.3
    #Click Element    xpath:/html/body/harbor-app/harbor-shell/clr-main-container/div/div/projects/div/div/list-project/clr-datagrid/div[1]/div/div/div/div/clr-dg-row/div/div[1]/div/clr-checkbox-wrapper/label
    
    Click Element    xpath:/html/body/harbor-app/harbor-shell/clr-main-container/div/div/projects/div/div/list-project/clr-datagrid/div[1]/div/div/div/div/div/div/div/div[1]/div/div[1]/label
    Sleep    ${tempo}

Select A Harbor Project Name
    [Arguments]    ${name}
    Click Element    xpath://html/body/harbor-app/harbor-shell/clr-main-container/div/div/projects/div/div/div[2]/div/hbr-filter/span/clr-icon
    Sleep    ${tempo}
    Input Selected Project Name    ${name}
    Sleep    ${tempo}
    Click Element    xpath:/html/body/harbor-app/harbor-shell/clr-main-container/div/div/projects/div/div/list-project/clr-datagrid/div[1]/div/div/div/div/clr-dg-row/div/div[2]/div/clr-dg-cell[1]/a
    Wait Until Element Contains    xpath:/html/body/harbor-app/harbor-shell/clr-main-container/div/div/project-detail/clr-tabs/ul/li[2]/button/a    Repositories    3sec
    Sleep    ${tempo}

Select A Harbor Repository
    [Arguments]    ${reponame}
    Click Element    xpath:/html/body/harbor-app/harbor-shell/clr-main-container/div/div/project-detail/hbr-repository-gridview/div/div[1]/div/div/div/hbr-filter/span/clr-icon    
    Sleep    ${tempo}
    Input Selected Repository Name    ${reponame}
    Sleep    ${tempo}
    Click Element    xpath:/html/body/harbor-app/harbor-shell/clr-main-container/div/div/project-detail/hbr-repository-gridview/div/div[2]/div/clr-datagrid/div[1]/div/div/div/div/clr-dg-row/div/div[2]/div/clr-dg-cell[1]/a
    Wait Until Element Contains    xpath://*[@id="repo-image"]    Artifacts    3sec
    Sleep    ${tempo}

Select A Harbor Artifact
    [Arguments]
    Click Element    xpath:/html/body/harbor-app/harbor-shell/clr-main-container/div/div/artifact-list-page/div/artifact-list/section[2]/div/section[2]/div/artifact-list-tab/div/div[2]/clr-datagrid/div[1]/div/div/div/div/clr-dg-row/div/div[2]/div/clr-dg-cell[1]/div/a
    Wait Until Element Contains    xpath:/html/body/harbor-app/harbor-shell/clr-main-container/div/div/artifact-summary/artifact-tag/h4    Tags    3sec
    Sleep    ${tempo}
