*** Settings ***
Documentation     A resource file with Web page related keywords.
Library           Process
Library           SeleniumLibrary

*** Keywords ***
Clear Input field
    [Arguments]    ${seleniumLocator}
    [Documentation]    ASCII Table: http://www.unix-manuals.com/refs/misc/ascii-table.html
    ...    CTRL-A -> 1
    ...    DEL (Delete) -> 127
    ...    Backspace -> 8
    #Press Key    ${seleniumLocator}    \\1
    #Press Key    ${seleniumLocator}    \\127
    Type Input based on XPath    ${seleniumLocator}    -
    Press Keys    ${seleniumLocator}    BACK_SPACE

Click Button By Text
    [Arguments]     ${text}
    ${locator}=     Wait For Element    ${text}    button
    Wait Until Element Is Enabled    ${locator}    2s
    Click Button    ${locator}

Click Preceding Icon
    [Arguments]    ${text_beside}
    ${unfold_icon}=    Wait For Element    ${text_beside}    h5    wait=15s
    Click Element    ${unfold_icon}

Click Preceding Icon2
    [Arguments]    ${locator}
    ${unfold_icon}=    Wait Until Element is Visible    ${locator}    5s
    Click Element    ${locator}

Copy Text To Clipboard
    [Arguments]    ${text}
    ${status}  ${message} =    Run Keyword And Ignore Error    Variable Should Exist  ${Local}
    Run Keyword If  "${status}" == "FAIL"    Set Global Variable  ${Local}    False

    Run Keyword If    ${Local}    Run Process    echo "${text}"| clip    shell=yes
    ...    ELSE    Run Process    bash    -c
    ...    printf '%s' "${text}" | xclip -sel clip
    ...    stdout=${output_dir}${/}dummyout     stderr=${output_dir}${/}dummyerr

Discard Popup
    [Arguments]    ${text}
    Run Keyword and Ignore Error    Expect Popup    ${text}

Download Should Be Done
    [Arguments]    ${directory}
    [Documentation]    Verifies that the directory has only one folder and it is not a temp file.
    ...    Returns path to the file
    ${files}    OperatingSystem.List Files In Directory    ${directory}
    Length Should Be    ${files}    1    Should be only one file in the download folder
    Should Not Match Regexp    ${files[0]}    (?i).*\\.(tmp|crdownload)    Chrome is still downloading a file
    ${file}    Join Path    ${directory}    ${files[0]}
    Log    File was successfully downloaded to ${file}
    [Return]    ${file}

Expect Popup
    [Arguments]    ${text}
    ${popup}=    Wait For Element    ${text}    wait=10s
    Wait Until Element is Visible    ${popup}    2s
    Click Element    ${popup}

Expect Error Popup
    ${error_popup_location}    Set Variable    xpath://*[@id="toast-container"]/div/i
    Wait Until Element is Visible    ${error_popup_location}    8s
    Click Element    ${error_popup_location}

Expect Success KeyCloak Popup
    Wait Until Element Is Visible    xpath://div[contains(@class,'alert-success')]    3s

Get Locator
    [Documentation]    Returns an XPATH expression to find element with tag (or xpath of tags)
    ...                matching to ${type} that contains the given ${text}
    [Arguments]    ${text}    ${type}=*    ${postfix}=${EMPTY}
    Return From Keyword    xpath://${type}\[text()[contains(.,'${text}')]]${postfix}

Get Exact Locator
    [Documentation]    Returns an XPATH expression to find element with tag (or xpath of tags)
    ...                matching to ${type} whose value equals the given ${text}
    [Arguments]    ${text}    ${type}=*    ${postfix}=${EMPTY}
    Return From Keyword    xpath://${type}[text()[contains(.,'${text}')]]${postfix}

Get Table Row
    [Arguments]    ${table_id}    ${text}    ${wait}=1s
    ${locator}=    Set Variable    xpath://${table_id}//tr[contains(.,'${text}')]
    Wait Until Element is Visible    ${locator}    ${wait}
    ${row}=    Get WebElement    ${locator}
    [Return]    ${row}

Login to GUI Artifactory
    [Arguments]    ${user}    ${password}
    Wait For Locator And Input Text    xpath://*[@id="user"]    ${user}    1m
    Wait For Locator And Input Text    xpath://*[@id="password"]    ${password}
    Sleep    5s
    Wait For Locator And Click On Button    xpath://button[contains(.,'Log In')]

Login to GUI Keycloak
    [Arguments]    ${user}    ${password}
    Wait For Locator And Input Text    xpath://input[@id="target2"]    ${user}    1m
    Wait For Locator And Input Text    xpath://input[@id="login-password"]    ${password}
    Sleep    5s
    Wait For Locator And Click On    xpath://input[@id="sbtbtn"]

Login to GUI Launchpad
    [Arguments]    ${user}    ${password}
    Wait For Locator And Input Text    xpath://input[contains(@name,'username')]    ${user}    1m
    Wait For Locator And Input Text    xpath://input[contains(@name,'password')]    ${password}
    Sleep    5s
    Wait For Locator And Click On Button    xpath://button[@id="csfWidgets-loginscreen-1-signIn-button"]

Open Browser To Login Page
    [Arguments]    ${url}    ${set_window_size}=True
    Open Normal Browser To Login Page    ${url}
    Go To    ${url}

Open Normal Browser To Login Page
    [Arguments]    ${url}
    ${status}  ${message} =    Run Keyword And Ignore Error    Variable Should Exist  ${Driver}
    Run Keyword If  "${status}" == "FAIL"    Set Global Variable  ${Driver}    Chrome
    Run Keyword If    '${Driver}' == 'firefox'    Use Firefox
    ...    ELSE    Use Chrome
    Set Selenium Speed    ${DELAY}
    Set Window Size    @{WIDE_SCREEN}

Select Element By Text
    [Arguments]     ${text}     ${elementType}=*
    ${locator}=    Wait For Element    ${text}    ${elementType}
    Click Element    ${locator}

Set Chrome Options
    [Documentation]    Set Chrome options for headless and headful mode
    ${options}=    Evaluate    sys.modules['selenium.webdriver'].ChromeOptions()    sys, selenium.webdriver
    &{prefs}    Create Dictionary
    ...    download.default_directory=${DOWNLOAD_DIR}
    ...    download.prompt_for_download=${False}
    ...    safebrowsing.enabled=false
    : FOR    ${option}    IN    @{chrome_arguments}
    \    Call Method    ${options}    add_argument    ${option}
    Call Method    ${options}    add_experimental_option    prefs    ${prefs}
    Call Method    ${options}    add_argument    --disable-gpu
    Call Method    ${options}    add_argument    --no-sandbox
    Call Method    ${options}    add_argument    --headless
    Call Method    ${options}    add_argument    --disable-dev-shm-usage
    Call Method    ${options}    add_argument    --no-proxy-server
    Call Method    ${options}    add_argument    --ignore-certificate-errors 
    [Return]    ${options}

Scroll Up
    Scroll To Location    0    0

Scroll To
    [Arguments]    ${locator}
    ${x}=    Get Horizontal Position    ${locator}
    ${y}=    Get Vertical Position    ${locator}
    Scroll To Location    ${x}   ${y-60}    # 60: the height of the blue header

Scroll To Location
    [Arguments]    ${x}    ${y}
    Execute JavaScript    window.scrollTo(${x}, ${y})

Take Screenshot from NCD
    ${tc_name}=    Get Variable Value    ${TEST NAME}    unknown-tc
    ${img_name}=    Capture Page Screenshot    ${tc_name}-{index}.png
    Image Converter    ${img_name}

Use Firefox
    Create Webdriver    Firefox

Use Chrome
    ${chrome_options}=    Set Chrome Options
    Create Webdriver    Chrome    chrome_options=${chrome_options}

Wait For Element
    [Arguments]    @{args}    &{kwargs}
    ${wait}=    Set Variable If    "wait" in &{kwargs}
    ...    ${kwargs.wait}    20s
    ${locator}=    Get Locator    @{args}
    Wait Until Page Contains Element    ${locator}    ${wait}
    Scroll To    ${locator}
    Wait Until Element is Visible    ${locator}    ${wait}
    [Return]    ${locator}

Wait For Element With Page Reload
    [Arguments]    @{args}    &{kwargs}
    ${wait}=    Set Variable If    "wait" in &{kwargs}
    ...    ${kwargs.wait}    6s
    ${number_of_attempts}=    Set Variable If    "number_of_attempts" in &{kwargs}
    ...    ${kwargs.number_of_attempts}    3
    ${locator}=    Get Locator    @{args}
    Wait Until Page Contains Element With Page Reload    ${locator}    ${wait}    ${number_of_attempts}
    Scroll To    ${locator}
    Wait Until Element is Visible With Page Reload    ${locator}    ${wait}    ${number_of_attempts}
    [Return]    ${locator}

Wait For Locator
    [Arguments]    ${locator}    ${wait}=5s
    Wait Until Page Contains Element    ${locator}    ${wait}
    Wait Until Element is Visible    ${locator}    ${wait}
    Scroll To    ${locator}
    [Return]    ${locator}

Wait For Locator And Click On
    [Arguments]    ${locator}    ${wait}=5s
    Wait For Locator    ${locator}    ${wait}
    Click Element    ${locator}

Wait For Locator And Click On Button
    [Arguments]    ${locator}    ${wait}=5s
    Wait For Locator    ${locator}    ${wait}
    Click Button    ${locator}

Wait For Locator And Input Text
    [Arguments]    ${locator}    ${input}    ${wait}=5s
    Wait For Locator    ${locator}    ${wait}
    Input Text    ${locator}    ${input}

Wait For Locator And Select From List By Value
    [Arguments]    ${locator}    ${value}    ${wait}=5s
    Wait For Locator    ${locator}    ${wait}
    Select From List By Value    ${locator}    ${value}

Wait Until Element Disappears
    [Arguments]    ${locator}    ${timeout}
    # Replacing robot selenium's 'Wait Until Element Is Not Visible'
    # as it randomly throws StaleElementException
    Wait Until Keyword Succeeds    ${timeout}    3s
    ...    Element Should Not Be Visible     ${locator}

Wait Until Element Is Visible With Page Reload
    [Arguments]    ${locator}    ${wait}=5s    ${number_of_attempts}=3
    :FOR    ${i}    IN RANGE    ${number_of_attempts}
    \    ${present}=  Run Keyword And Return Status    Wait Until Element is Visible   ${locator}    ${wait}
    \    Exit For Loop If   ${present}
    \    Reload Page
    Should Be True    ${present}

Wait Until Page Contains Element With Page Reload
    [Arguments]    ${locator}    ${wait}=6s    ${number_of_attempts}=1
    :FOR    ${i}    IN RANGE    ${number_of_attempts}
    \    Reload Page
    \    ${present}=  Run Keyword And Return Status    Wait Until Page Contains Element   ${locator}
    \    Exit For Loop If   ${present}
    \    Sleep     ${wait}
    Should Be True    ${present}

Wait Until Product Appears
    [Arguments]    ${product}
    ${locator}=    Get Locator    ${product}     option
    Wait Until Element Is Visible With Page Reload    ${locator}    2s    10