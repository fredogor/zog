*** Settings ***
Library    OperatingSystem

*** Keywords ***
List Docker Images On Local Machine
    [Arguments]
    [Documentation]    pour lister les images Docker presentes sur la machine Linux locale
    ${list}=    Run    docker images
    Log    ${list}
    [return]    ${list}

Remove A Docker Image On Local Machine
    [Arguments]    ${ref_image}
    [Documentation]    pour supprimer une image docker sur la machine Linux
    ${output}=    Run    docker rmi -f ${ref_image}
    Log    ${output}
    [return]    ${output}

Pull Docker Image On Local Machine
    [Arguments]    ${image_name}
    [Documentation]    pour puller une image Docker sur la machine Linux locale
    ${output}=    Run    docker pull ${image_name}
    Log    ${output}
    [return]    ${output}

Tag Docker Image On Local Machine
    [Arguments]    ${image_name}    ${version_n}    ${version_p}    ${harbor_url}    ${project_name}
    [Documentation]    pour tagguer l'image Docker sur la machine Linux locale
    ${output}=    Run    docker tag ${image_name}:${version_n} ${harbor_url}/${project_name}/${image_name}:${version_p}
    Log    ${output}
    [return]    ${output}

Login On Harbor
    [Arguments]    ${harbor_login}    ${harbor_passwd}    ${harbor_url}
    [Documentation]    pour se logguer sur Harbor
    ${output}=    Run    docker login -u ${harbor_login} -p ${harbor_passwd} ${harbor_url}
    Log    ${output}
    [return]    ${output}

Logout On Harbor
    [Arguments]    ${harbor_url}
    [Documentation]    pour se delogguer sur Harbor
    ${output}=    Run    docker logout ${harbor_url}
    Log    ${output}
    [return]    ${output}

Push Docker Image On Harbor Registry
    [Arguments]    ${image_name}    ${version}    ${harbor_url}    ${project_name}
    [Documentation]    pour tagguer l'image Docker sur la machine Linux locale
    ${output}=    Run    docker push ${harbor_url}/${project_name}/${image_name}:${version}
    Log    ${output}
    [return]    ${output}

Create Harbor Project
    [Arguments]    ${harbor_url}    ${login}    ${passwd}    ${project}
    [Documentation]    to create a Harbor project
    ${rc}=    Run    curl -k -X POST --user ${login}:${passwd} "https://${harbor_url}/api/v2.0/projects" -H "accept: application/json" -H "Content-Type: application/json" -d '{"project_name": "${project}", "public": true}'
    Log    ${rc}
    [return]    ${rc}

Remove Harbor Project
    [Arguments]    ${harbor_url}    ${login}    ${passwd}    ${project_name}
    [Documentation]    to remove a Harbor project
    ${rc}=    Run    curl -k -X DELETE --user ${login}:${passwd} "https://${harbor_url}/api/v2.0/projects/${project_name}" -H "accept: application/json"
    Log    ${rc}
    [return]    ${rc}
    
Get Harbor Project ID From Harbor Project Name
    [Arguments]    ${harbor_url}    ${project_name}
    [Documentation]    to get IDs of harbor project from project name
    ...    we search the project ID corresponding to the project name
    ...    we handle 2 lists : list of ID and list of names
    ${ids}=    Run    curl -k -X GET "https://${harbor_url}/api/v2.0/projects" -H "accept: application/json" | jq '.[]' | jq '.project_id'
    ${names}=    Run    curl -k -X GET "https://${harbor_url}/api/v2.0/projects" -H "accept: application/json" | jq '.[]' | jq '.name'
    @{IDS}=    Split String    ${ids}
    @{NAMES}=    Split String    ${names}
    
    ${i}    Set variable    ${0}
    FOR    ${name}    IN    @{NAMES}
        IF    ${name} == '${project_name}'
        Run keyword    Log    ${name}
        Run keyword    Log    ${i}
        ${id}    Set variable    ${i}
        END
    ${i}    Set variable    ${i + 1}
    END
    Log    ${id}
    Log    ${ids}[${id}]
    [return]    ${ids}[${id}]

Get All Harbor Project Names
    [Arguments]    ${harbor_url}
    [Documentation]    to get names of all the Harbor projects
    ${names}=    Run    curl -k -X GET "https://${harbor_url}/api/v2.0/projects" -H "accept: application/json" | jq '.[]' | jq '.name'
    Log    ${names}
    [return]    ${names}

Get All Harbor Project IDs
    [Arguments]    ${harbor_url}
    [Documentation]    to get IDs of all the Harbor projects
    ${ids}=    Run    curl -k -X GET "https://${harbor_url}/api/v2.0/projects" -H "accept: application/json" | jq '.[]' | jq '.project_id'
    Log    ${ids}
    [return]    ${ids}

Get Harbor Project Infos
    [Arguments]    ${harbor_url}    ${id}
    [Documentation]    to get infos of a Harbor project from the project ID
    ${infos}=    Run    curl -k -X GET "https://${harbor_url}/api/v2.0/projects/${id}" -H "accept: application/json" | jq .
    Log    ${infos}
    [return]    ${infos}
    
Get Harbor Repositories Of A Project
    [Arguments]    ${harbor_url}    ${project}
    [Documentation]    to get all the repositories of a Harbor project
    ${repos}=    Run    curl -k -X GET "https://${harbor_url}/api/v2.0/projects/${project}/repositories" -H "accept: application/json" | jq '.[]' | jq '.name'
    Log    ${repos}
    [return]    ${repos}

Remove Harbor Repository Of A Project
    [Arguments]    ${harbor_url}    ${login}    ${passwd}    ${project}    ${repo}
    [Documentation]    to remove a repository of a Harbor project
    ${rc}=    Run    curl -k -X DELETE --user ${login}:${passwd} "https://${harbor_url}/api/v2.0/projects/${project}/repositories/${repo}" -H "accept: application/json"
    Log    ${rc}
    [return]    ${rc}

Get Infos Of A Harbor Repository
    [Arguments]    ${harbor_url}    ${project}    ${repo}
    [Documentation]    to get infos of a repository of a Harbor project
    ${infos}=    Run    curl -k -X GET "https://${harbor_url}/api/v2.0/projects/${project}/repositories/${repo}" -H "accept: application/json" | jq .
    Log    ${infos}
    [return]    ${infos}

Get List Of Artifacts Of A Harbor Repository
    [Arguments]    ${harbor_url}    ${project}    ${repo}
    [Documentation]    to get the list of artifacts of a repository in a Harbor project
    ${infos}=    Run    curl -k -X GET "https://${harbor_url}/api/v2.0/projects/${project}/repositories/${repo}/artifacts" -H "accept: application/json" | jq '.[0]'
    Log    ${infos}
    [return]    ${infos}

Check If Harbor Registry Implements OCI Specification
    [Arguments]    ${harbor_url}    ${login}    ${passwd}
    [Documentation]    to remove a repository of a Harbor project
    ${output}=    Run    curl -k -X GET --user ${login}:${passwd} "https://${harbor_url}/v2/" -H "accept: application/json" | jq .
    Log    ${output}
    [return]    ${output}

List Tags Of A Repo In A Harbor Project
    [Arguments]    ${harbor_url}    ${login}    ${passwd}    ${project_name}    ${repo_name}
    [Documentation]    to list all the tags of a repo in a Harbor project
    ${output}=    Run    curl -k -X GET --user ${login}:${passwd} "https://${harbor_url}/v2/${project_name}/${repo_name}/tags/list" -H "accept: application/json" | jq .
    Log    ${output}
    [return]    ${output}

