*** Settings ***
Resource    ../../resources/harbor-cli/harbor-curl.robot

*** Variables ***
${docker_image_1}    ubuntu
${docker_image_2}    debian
${docker_image_3}    alpine

${image_version_1}    latest
${image_version_2}    1.2.3
${image_version_3}    4.5.6

${project_name_1}    fog
${project_name_2}    gromit

*** Test Cases ***
List Docker Images On Local Machine
    [Tags]    docker    setup
    [Documentation]    on liste les images docker sur la machine locale
    
    ${output}=    List Docker Images On Local Machine
    Should Not Contain    ${output}    ubuntu
    Should Not Contain    ${output}    debian
    Should Not Contain    ${output}    alpine

Create A Project On Harbor
    [Tags]    setup
    [Documentation]    on crée un projet harbor sur le serveur harbor

    Create Harbor Project    ${harbor_url}    ${harbor_login}    ${harbor_passwd}    ${project_name_1}
    Get All Harbor Project Names    ${harbor_url}

Pull Docker Images On Local Machine
    [Tags]    docker    setup
    [Documentation]    on pulle 3 images docker : ubuntu, debian, alpine

    Pull Docker Image On Local Machine    ${docker_image_1}
    Pull Docker Image On Local Machine    ${docker_image_2}
    Pull Docker Image On Local Machine    ${docker_image_3}
    ${output}=    List Docker Images On Local Machine
    Should Contain    ${output}    ubuntu
    Should Contain    ${output}    debian
    Should Contain    ${output}    alpine

Tag Docker Images On Local Machine
    [Tags]    docker    setup
    [Documentation]    on tagge les 3 images docker : ubuntu, debian, alpine

    Tag Docker Image On Local Machine    ${docker_image_1}    ${image_version_1}    ${image_version_1}    ${harbor_url}    ${project_name_1}
    Tag Docker Image On Local Machine    ${docker_image_2}    ${image_version_1}    ${image_version_1}    ${harbor_url}    ${project_name_1}
    Tag Docker Image On Local Machine    ${docker_image_3}    ${image_version_1}    ${image_version_1}    ${harbor_url}    ${project_name_1}
    ${output}=    List Docker Images On Local Machine
    Should Contain    ${output}    ${harbor_url}/${project_name_1}/ubuntu
    Should Contain    ${output}    ${harbor_url}/${project_name_1}/debian
    Should Contain    ${output}    ${harbor_url}/${project_name_1}/alpine

Push Docker Images On Harbor
    [Tags]    docker    setup
    [Documentation]    on pushe les 3 images docker taggées (ubuntu, debian, alpine) sur harbor

    ${output}=    Login On Harbor    ${harbor_login}    ${harbor_passwd}    ${harbor_url}
    Should Contain    ${output}    Login Succeeded
    ${output}=    Push Docker Image On Harbor Registry    ${docker_image_1}    ${image_version_1}    ${harbor_url}    ${project_name_1}
    Should Contain    ${output}    latest: digest: sha256
    ${output}=    Push Docker Image On Harbor Registry    ${docker_image_2}    ${image_version_1}    ${harbor_url}    ${project_name_1}
    Should Contain    ${output}    latest: digest: sha256
    ${output}=    Push Docker Image On Harbor Registry    ${docker_image_3}    ${image_version_1}    ${harbor_url}    ${project_name_1}
    Should Contain    ${output}    latest: digest: sha256
    ${output}=    Logout On Harbor    ${harbor_url}
    Should Contain    ${output}    Removing login credentials

List All The Projects On Harbor
    [Tags]    harbor
    [Documentation]    on liste tous les projets sur harbor

    ${output}=    Get All Harbor Project Names    ${harbor_url}
    Should Contain    ${output}    ${project_name_1}

List All The Repositories Of A Project On Harbor
    [Tags]    harbor
    [Documentation]    on liste tous les repositories d'un projet sur harbor

    ${output}=    Get Harbor Repositories Of A Project    ${harbor_url}    ${project_name_1}
    Should Contain    ${output}    ubuntu
    Should Contain    ${output}    debian
    Should Contain    ${output}    alpine

Remove A Repository Of A Project On Harbor
    [Tags]    harbor
    [Documentation]    on supprime le repository (alpine) d'un projet sur harbor

    Remove Harbor Repository Of A Project    ${harbor_url}    ${harbor_login}    ${harbor_passwd}    ${project_name_1}    ${docker_image_3}
    ${output}=    Get Harbor Repositories Of A Project    ${harbor_url}    ${project_name_1}
    Should Not Contain    ${output}    alpine

Add A Repository Of A Project On Harbor
    [Tags]    harbor
    [Documentation]    on rajoute le repository (alpine) dans le projet sur harbor

    ${output}=    Login On Harbor    ${harbor_login}    ${harbor_passwd}    ${harbor_url}
    Should Contain    ${output}    Login Succeeded
    ${output}=    Push Docker Image On Harbor Registry    ${docker_image_3}    ${image_version_1}    ${harbor_url}    ${project_name_1}
    Should Contain    ${output}    latest: digest: sha256
    ${output}=    Logout On Harbor    ${harbor_url}
    Should Contain    ${output}    Removing login credentials
    ${output}=    Get Harbor Repositories Of A Project    ${harbor_url}    ${project_name_1}
    Should Contain    ${output}    alpine

Create A New Project On Harbor
    [Tags]    harbor
    [Documentation]    on crée un nouveau projet

    Create Harbor Project    ${harbor_url}    ${harbor_login}    ${harbor_passwd}    ${project_name_2}
    Get Harbor Project Infos    ${harbor_url}    ${project_name_2}
    ${output}=    Get All Harbor Project Names    ${harbor_url}
    Should Contain    ${output}    gromit

Remove The New Project On Harbor
    [Tags]    harbor
    [Documentation]    on supprime le nouveau projet

    Remove Harbor Project    ${harbor_url}    ${harbor_login}    ${harbor_passwd}    ${project_name_2}
    ${output}=    Get All Harbor Project Names    ${harbor_url}
    Should Not Contain    ${output}    gromit

List Artifacts Of Several Repositories On Harbor
    [Tags]    harbor
    [Documentation]    on liste les repositories d'un projet harbor

    Get List Of Artifacts Of A Harbor Repository    ${harbor_url}    ${project_name_1}    ${docker_image_1}
    Get List Of Artifacts Of A Harbor Repository    ${harbor_url}    ${project_name_1}    ${docker_image_2}

Check If Harbor Respects OCI Requirements
    [Tags]    harbor
    [Documentation]    on vérifie que harbor est conforme au standard OCI

    Check If Harbor Registry Implements OCI Specification    ${harbor_url}    ${harbor_login}    ${harbor_passwd}

Get Infos Of Repositories On Harbor
    [Tags]    harbor
    [Documentation]    on récupère les infos des repositories d'un projet harbor

    Get Infos Of A Harbor Repository    ${harbor_url}    ${project_name_1}    ${docker_image_1}
    Get Infos Of A Harbor Repository    ${harbor_url}    ${project_name_1}    ${docker_image_2}

Add Tagged Images On Harbor
    [Tags]    docker    setup
    [Documentation]    on rajoute des images tagguées à un projet harbor

    Tag Docker Image On Local Machine    ${docker_image_1}    ${image_version_1}    ${image_version_2}    ${harbor_url}    ${project_name_1}
    Tag Docker Image On Local Machine    ${docker_image_1}    ${image_version_1}    ${image_version_3}    ${harbor_url}    ${project_name_1}

    ${output}=    Login On Harbor    ${harbor_login}    ${harbor_passwd}    ${harbor_url}
    Should Contain    ${output}    Login Succeeded
    Push Docker Image On Harbor Registry    ${docker_image_1}    ${image_version_2}    ${harbor_url}    ${project_name_1}
    Push Docker Image On Harbor Registry    ${docker_image_1}    ${image_version_3}    ${harbor_url}    ${project_name_1}
    ${output}=    Logout On Harbor    ${harbor_url}
    Should Contain    ${output}    Removing login credentials

List Tags Of Repository On Harbor
    [Tags]    harbor
    [Documentation]    on liste les tags des repositories d'un projet harbor

    List Tags Of A Repo In A Harbor Project    ${harbor_url}    ${harbor_login}    ${harbor_passwd}    ${project_name_1}    ${docker_image_1}
    List Tags Of A Repo In A Harbor Project    ${harbor_url}    ${harbor_login}    ${harbor_passwd}    ${project_name_1}    ${docker_image_2}

Remove A Project On Harbor
    [Tags]    teardown
    [Documentation]    on supprime un projet sur harbor :
    ...    on supprime d'abord les repositories
    ...    on suprime ensuite le projet

    Remove Harbor Repository Of A Project    ${harbor_url}    ${harbor_login}    ${harbor_passwd}    ${project_name_1}    ${docker_image_1}
    Remove Harbor Repository Of A Project    ${harbor_url}    ${harbor_login}    ${harbor_passwd}    ${project_name_1}    ${docker_image_2}
    Remove Harbor Repository Of A Project    ${harbor_url}    ${harbor_login}    ${harbor_passwd}    ${project_name_1}    ${docker_image_3}
    ${output}=    Get Harbor Repositories Of A Project    ${harbor_url}    ${project_name_1}
    Should Not Contain    ${output}    ubuntu
    Should Not Contain    ${output}    debian
    Should Not Contain    ${output}    alpine

    Remove Harbor Project    ${harbor_url}    ${harbor_login}    ${harbor_passwd}    ${project_name_1}
    ${output}=    Get All Harbor Project Names    ${harbor_url}
    Should Not Contain    ${output}    ${project_name_1}

Remove Docker Images On Local Machine
    [Tags]    docker    setup
    [Documentation]    on supprime toutes les images docker du projet sur la machine locale /!\
    
    List Docker Images On Local Machine
    Remove A Docker Image On Local Machine    ${docker_image_1}
    Remove A Docker Image On Local Machine    ${docker_image_2}
    Remove A Docker Image On Local Machine    ${docker_image_3}
    Remove A Docker Image On Local Machine    ${harbor_url}/${project_name_1}/${docker_image_1}:${image_version_1}
    Remove A Docker Image On Local Machine    ${harbor_url}/${project_name_1}/${docker_image_1}:${image_version_2}
    Remove A Docker Image On Local Machine    ${harbor_url}/${project_name_1}/${docker_image_1}:${image_version_3}
    Remove A Docker Image On Local Machine    ${harbor_url}/${project_name_1}/${docker_image_2}:${image_version_1}
    Remove A Docker Image On Local Machine    ${harbor_url}/${project_name_1}/${docker_image_3}:${image_version_1}
    ${output}=    List Docker Images On Local Machine
    Should Not Contain    ${output}    ubuntu
    Should Not Contain    ${output}    debian
    Should Not Contain    ${output}    alpine
    Should Not Contain    ${output}    ${harbor_url}/${project_name_1}/ubuntu
    Should Not Contain    ${output}    ${harbor_url}/${project_name_1}/ubuntu:1.2.3
    Should Not Contain    ${output}    ${harbor_url}/${project_name_1}/ubuntu:4.5.6 
    Should Not Contain    ${output}    ${harbor_url}/${project_name_1}/debian
    Should Not Contain    ${output}    ${harbor_url}/${project_name_1}/alpine

