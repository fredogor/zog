*** Settings ***
Resource    ../../resources/harbor-gui/web-harbor.robot

*** Variables ***
${browser1}    firefox
${browser2}    chrome
${tempo}    5sec
${usermail}    foggy@gmail.com
${realname}    fog gy

*** Test Cases ***
Harbor Account Creation
    [Tags]    Harbor Account
    Open Browser Gui Harbor    ${harbor_url}    ${browser1}
    Click Set Up For An Account
    Input Username    ${harbor_login}
    Input Usermail    ${usermail}
    Input Realname    ${realname}
    Input Newpassword    ${harbor_passwd}
    Input Confirmpassword    ${harbor_passwd}
    Click Sign Up
    Sleep    ${tempo}
    Login to GUI Harbor    ${harbor_login}    ${harbor_passwd}
    Sleep    ${tempo}
    Logout GUI Harbor
    Close Browser
    
*** Keywords ***
Open Browser Gui Harbor
    [Arguments]    ${harbor_url}    ${browser}
    Open Browser    https://${harbor_url}    ${browser}
    Wait Until Page Contains    Harbor    30

Click Set Up For An Account
    Wait For Locator And Click On    xpath:/html/body/harbor-app/sign-in/clr-main-container/div/form/div[1]/a

Click Sign Up
    Wait For Locator And Click On    xpath://*[@id="sign-up"]

Input Username
    [Arguments]    ${username}
    Wait For Locator And Input Text    xpath://*[@id="username"]    ${username}

Input Usermail
    [Arguments]    ${username}
    Wait For Locator And Input Text    xpath://*[@id="email"]    ${usermail}

Input Realname
    [Arguments]    ${realname}
    Wait For Locator And Input Text    xpath://*[@id="realname"]    ${realname}

Input Newpassword
    [Arguments]    ${password}
    Wait For Locator And Input Text    xpath://*[@id="newPassword"]    ${password}

Input Confirmpassword
    [Arguments]    ${password}
    Wait For Locator And Input Text    xpath://*[@id="confirmPassword"]    ${password}

