*** Settings ***
Resource    ../../resources/harbor-gui/web-harbor.robot

*** Variables ***
${browser1}    firefox
${browser2}    chrome
${tempo}    3sec
${DELAY}    0
${username}    foggy
${password}    Ouessant##29
${project_name}    fog
${image_name}    ubuntu

*** Test Cases ***
Harbor gui login
    [Tags]    GUI
    Open Browser    https://${harbor_url}    ${browser1}
    Wait Until Page Contains    Harbor    30
    Login to GUI Harbor    ${username}    ${password}
    Sleep    ${tempo}
    Close Browser

Harbor gui logout
    [Tags]    GUI
    Open Browser    https://${harbor_url}    ${browser1}
    Wait Until Page Contains    Harbor    30
    Login to GUI Harbor    ${username}    ${password}
    Sleep    ${tempo}
    Logout GUI Harbor
    Sleep    ${tempo}
    Close Browser

Harbor gui version
    [Tags]    GUI
    Open Browser    https://${harbor_url}    ${browser1}
    Wait Until Page Contains    Harbor    30
    Login to GUI Harbor    ${username}    ${password}
    Sleep    ${tempo}
    Get Harbor Version
    Sleep    ${tempo}
    Close Browser

Harbor gui user profile
    [Tags]    GUI
    Open Browser    https://${harbor_url}    ${browser1}
    Wait Until Page Contains    Harbor    30
    Login to GUI Harbor    ${username}    ${password}
    Sleep    ${tempo}
    Get Harbor User Profile
    Sleep    ${tempo}
    Close Browser

Harbor gui change admin password
    [Tags]    GUI
    Open Browser    https://${harbor_url}    ${browser1}
    Wait Until Page Contains    Harbor    30
    Login to GUI Harbor    ${username}    ${password}
    Sleep    ${tempo}
    Change Harbor Admin Password
    Sleep    ${tempo}
    Close Browser

Harbor gui project creation
    [Tags]    GUI
    Open Browser    https://${harbor_url}    ${browser1}
    Wait Until Page Contains    Harbor    30
    Login to GUI Harbor    ${username}    ${password}
    Sleep    ${tempo}
    Create A Harbor Project    gromit    public
    Close Browser

Harbor gui project removing
    [Tags]    GUI
    Open Browser    https://${harbor_url}    ${browser1}
    Wait Until Page Contains    Harbor    30
    Login to GUI Harbor    ${username}    ${password}
    Sleep    ${tempo}
    Select A Harbor Project    gromit
    Remove A Harbor Project    gromit

    Close Browser

Harbor gui project creation selection and removing
    [Tags]    GUI
    Open Browser    https://${harbor_url}    ${browser1}
    Wait Until Page Contains    Harbor    30
    Login to GUI Harbor    ${username}    ${password}
    Sleep    ${tempo}
    Create A Harbor Project    walace    public
    Select A Harbor Project    walace
    Remove A Harbor Project    walace
    Close Browser

Harbor gui project name selection
    [Tags]    GUI
    Open Browser    https://${harbor_url}    ${browser1}
    Wait Until Page Contains    Harbor    30
    Login to GUI Harbor    ${username}    ${password}
    Sleep    ${tempo}
    Select A Harbor Project Name    ${project_name}
    Close Browser

Harbor gui repository selection
    [Tags]    GUI
    Open Browser    https://${harbor_url}    ${browser1}
    Wait Until Page Contains    Harbor    30
    Login to GUI Harbor    ${username}    ${password}
    Sleep    ${tempo}
    Select A Harbor Project Name    ${project_name}
    Select A Harbor Repository    ${project_name}/${image_name}
    Close Browser

Harbor gui artifact selection
    [Tags]    GUI
    Open Browser    https://${harbor_url}    ${browser1}
    Wait Until Page Contains    Harbor    30
    Login to GUI Harbor    ${username}    ${password}
    Sleep    ${tempo}
    Select A Harbor Project Name    ${project_name}
    Select A Harbor Repository    ${project_name}/${image_name}
    Select A Harbor Artifact
    Close Browser
    
