*** Settings ***
Resource    ../../resources/portainer-cli/portainer-api.robot

*** Variables ***
${local_endpoint_name}    local
${local_endpoint_url}    ""

${endpoint_name_1}    PC-packard
${endpoint_url_1}    tcp://192.168.1.58:4243
${endpoint_name_2}    EliteDesk
${endpoint_url_2}    tcp://192.168.1.65:4243


*** Test Cases ***
Create Initial Admin Account
    [Tags]    setup
    [Documentation]    on crée le compte admin initial (login + mot de passe) pour portainer
    
    ${output}=    Create Initial Admin Account    ${portainer_url}    ${portainer_login}    ${portainer_passwd}
    Should Contain    ${output}    "Username": "admin"
    Should Contain    ${output}    "Id": 1
    
Add Local Endpoints In Portainer
    [Tags]    portainer
    [Documentation]    on rajoute un endpoint local à portainer

    ${token}=    Log In Portainer    ${portainer_url}    ${portainer_login}    ${portainer_passwd}
    ${output}=    Add An Endpoint In Portainer    ${portainer_url}    ${token}    ${local_endpoint_name}    ${local_endpoint_url}
    Should Contain    ${output}    "Id": 1
    Should Contain    ${output}    "Name": "local"

Add Remote Endpoint In Portainer
    [Tags]    portainer
    [Documentation]    on rajoute un endpoint distant à portainer

    ${token}=    Log In Portainer    ${portainer_url}    ${portainer_login}    ${portainer_passwd}
    ${output}=    Add An Endpoint In Portainer    ${portainer_url}    ${token}    ${endpoint_name_1}    ${endpoint_url_1}
    Should Contain    ${output}    "Name": "PC-packard"
    ${output}=    Add An Endpoint In Portainer    ${portainer_url}    ${token}    ${endpoint_name_2}    ${endpoint_url_2}
    Should Contain    ${output}    "Name": "EliteDesk"

List All The Endpoints Defined In Portainer
    [Tags]    portainer
    [Documentation]    on liste tous les endpoints de portainer

    ${token}=    Log In Portainer    ${portainer_url}    ${portainer_login}    ${portainer_passwd}
    ${output}=    List All The Endpoints In Portainer    ${portainer_url}    ${token}
    Should Contain    ${output}    "local"
    Should Contain    ${output}    "PC-packard"

List All The Endpoints Ids Defined In Portainer
    [Tags]    portainer
    [Documentation]    on liste tous les ids des endpoints de portainer

    ${token}=    Log In Portainer    ${portainer_url}    ${portainer_login}    ${portainer_passwd}
    List All The Ids Of The Endpoints In Portainer    ${portainer_url}    ${token}

List All The Endpoints Names Defined In Portainer
    [Tags]    portainer
    [Documentation]    on liste tous les endpoints de portainer

    ${token}=    Log In Portainer    ${portainer_url}    ${portainer_login}    ${portainer_passwd}
    ${output}=    List All The Names Of The Endpoints In Portainer    ${portainer_url}    ${token}
    Should Contain    ${output}    "local"
    Should Contain    ${output}    "PC-packard"

List The Local Endpoint In Portainer
    [Tags]    portainer
    [Documentation]    on liste le endpoint local de portainer

    ${token}=    Log In Portainer    ${portainer_url}    ${portainer_login}    ${portainer_passwd}
    ${output}=    List An Endpoint In Portainer    ${portainer_url}    ${token}    1
    Should Contain    ${output}    "Name": "local"

Recover An Endpoint Id Of Portainer
    [Tags]    portainer
    [Documentation]    on récupère un id d'un endpoint

    ${token}=    Log In Portainer    ${portainer_url}    ${portainer_login}    ${portainer_passwd}
    ${id1}=    Get Endpoint Id From Endpoint Name In Portainer    ${portainer_url}    ${token}    local
    ${id2}=    Get Endpoint Id From Endpoint Name In Portainer    ${portainer_url}    ${token}    PC-packard    
    ${id3}=    Get Endpoint Id From Endpoint Name In Portainer    ${portainer_url}    ${token}    EliteDesk

Remove A Remote Endpoint In Portainer
    [Tags]    portainer
    [Documentation]    on supprime un endpoint distant

    ${token}=    Log In Portainer    ${portainer_url}    ${portainer_login}    ${portainer_passwd}
    ${id}=    Get Endpoint Id From Endpoint Name In Portainer    ${portainer_url}    ${token}    PC-packard
    Remove An Endpoint In Portainer    ${portainer_url}    ${token}    ${id}
    ${output}=    List All The Names Of The Endpoints In Portainer    ${portainer_url}    ${token}
    Should Not Contain    ${output}    "Name": "PC-packard"

Add A New Remote Endpoint In Portainer
    [Tags]    portainer
    [Documentation]    on rajoute un endpoint distant à portainer

    ${token}=    Log In Portainer    ${portainer_url}    ${portainer_login}    ${portainer_passwd}
    Add An Endpoint In Portainer    ${portainer_url}    ${token}    ${endpoint_name_1}    ${endpoint_url_1}
    ${output}=    List All The Names Of The Endpoints In Portainer    ${portainer_url}    ${token}
    Should Contain    ${output}    "PC-packard"

List All The Containers Of Local Endpoint In Portainer
    [Tags]    portainer
    [Documentation]    on liste tous les containers du endpoint local

    ${token}=    Log In Portainer    ${portainer_url}    ${portainer_login}    ${portainer_passwd}
    ${id}=    Get Endpoint Id From Endpoint Name In Portainer    ${portainer_url}    ${token}    local
    List All The Containers Of An Endpoint In Portainer    ${portainer_url}    ${token}    ${id}

List All The Containers Of A Remote Endpoint In Portainer
    [Tags]    portainer
    [Documentation]    on liste tous les containers du endpoint local

    ${token}=    Log In Portainer    ${portainer_url}    ${portainer_login}    ${portainer_passwd}
    ${id}=    Get Endpoint Id From Endpoint Name In Portainer    ${portainer_url}    ${token}    PC-packard
    List All The Containers Of An Endpoint In Portainer    ${portainer_url}    ${token}    ${id}


